# Panorama Cake dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:panoramacake)

![panorama cake theme light screenshot](https://i.imgur.com/SUrp8sY.png)

![panorama cake theme dark screenshot](https://i.imgur.com/oWj3bJj.png)
